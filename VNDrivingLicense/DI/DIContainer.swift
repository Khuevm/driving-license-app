//
//  DIContainer.swift
//  dmv
//
//  Created by Thom Vu on 25/05/2021.
//

import Foundation
import Swinject

class DIContainer {
    static let shared = DIContainer()
    
    var container: Container!
    init() {
        self.container = Container()
        //Intro
        registerIntroViewModel()
        
        //Splash
        registerSplashViewModel()
        
        //Home
        registerHomeViewModel()
        
        //Traffic Sign
        registerSignViewModel()
        
        //Practice
        registerPracticeOverViewViewModel()
        registerPracticeViewModel()
        
        //Exam
        registerExamOverViewViewModel()
        registerExamViewModel()
        registerExamResultViewModel()
        registerExamResultDetailViewModel()
    }
    
    // MARK: - Intro
    func registerIntroViewModel() {
        self.container.register(IntroViewModelFactory.self) { _ in
            return IntroViewModel()
        }
    }
    
    // MARK: - Splash
    func registerSplashViewModel() {
        self.container.register(SplashViewModelFactory.self) { _ in
            return SplashViewModel()
        }
    }
    
    // MARK: - Home
    func registerHomeViewModel() {
        self.container.register(HomeViewModelFactory.self) { _ in
            return HomeViewModel()
        }
    }
    
    // MARK: - TrafficSign
    func registerSignViewModel() {
        self.container.register(SignViewModelFactory.self) { _ in
            return SignViewModel()
        }
    }
    
    // MARK: - Practice
    func registerPracticeOverViewViewModel() {
        self.container.register(PracticeOverViewViewModelFactory.self) { _ in
            return PracticeOverViewViewModel()
        }
    }
    
    func registerPracticeViewModel() {
        self.container.register(PracticeViewModelFactory.self) { _ in
            return PracticeViewModel()
        }
    }
    
    // MARK: - Exam
    func registerExamOverViewViewModel() {
        self.container.register(ExamOverviewViewModelFactory.self) { _ in
            return ExamOverviewViewModel()
        }
    }
    
    func registerExamViewModel() {
        self.container.register(ExamViewModelFactory.self) { _ in
            return ExamViewModel()
        }
    }
    
    func registerExamResultViewModel() {
        self.container.register(ExamResultViewModelFactory.self) { _ in
            return ExamResultViewModel()
        }
    }
    
    func registerExamResultDetailViewModel() {
        self.container.register(ExamResultDetailViewModelFactory.self) { _ in
            return ExamResultDetailViewModel()
        }
    }
}
