//
//  TestResult.swift
//  VNDrivingLicense
//
//  Created by Khue on 02/05/2022.
//

import UIKit
import FMDB

class TestResult: BaseEntity {
    var testID: Int!
    var numberAnswerCorrect: Int!
    var numberAnswerIncorrect: Int!
    var numberAnswerRemaining: Int!
    var totalQuestion: Int!
    var miniumAnswerCorrect: Int!
    var isFailDieQuestion: Int! //0:False - 1:True
    
    var testResultDetails: [TestResultDetail]?
    
    override init() {
        super.init()
    }
    
    override init(resultSet: FMResultSet) {
        super.init(resultSet: resultSet)
        
        self.testID = Int(resultSet.int(forColumn: "testID"))
        self.numberAnswerCorrect = Int(resultSet.int(forColumn: "numberAnswerCorrect"))
        self.numberAnswerIncorrect = Int(resultSet.int(forColumn: "numberAnswerIncorrect"))
        self.numberAnswerRemaining = Int(resultSet.int(forColumn: "numberAnswerRemaining"))
        self.totalQuestion = Int(resultSet.int(forColumn: "totalQuestion"))
        self.miniumAnswerCorrect = Int(resultSet.int(forColumn: "miniumAnswerCorrect"))
        self.isFailDieQuestion = Int(resultSet.int(forColumn: "isFailDieQuestion"))
    }
    
    override func saveUpdate() {
        super.saveUpdate()
        if DBController.shared.isExistRecord(query: "select 1 from TestResult where testID = \(self.testID!) limit 1") {
            let query = "update TestResult set numberAnswerCorrect = \(self.numberAnswerCorrect!), numberAnswerIncorrect = \(self.numberAnswerIncorrect!), numberAnswerRemaining = \(self.numberAnswerRemaining!), isFailDieQuestion = \(self.isFailDieQuestion!) where testID = \(self.testID!)"
            _ = DBController.shared.excuteUpdate(query: query)
        } else  {
            insertToDatabase()
        }
    }
    
    override func insertToDatabase() {
        let query = "insert into TestResult (testID, numberAnswerCorrect, miniumAnswerCorrect, totalQuestion, numberAnswerIncorrect, numberAnswerRemaining, isFailDieQuestion) values (\(self.testID!), \(numberAnswerCorrect!), \(self.miniumAnswerCorrect!), \(self.totalQuestion!), \(self.numberAnswerIncorrect!), \(self.numberAnswerRemaining!), \(self.isFailDieQuestion!))"
        _ = DBController.shared.excuteUpdate(query: query)
    }
    
    func isPassed() -> Bool {
        return numberAnswerCorrect >= miniumAnswerCorrect && isFailDieQuestion == 0
    }
}
