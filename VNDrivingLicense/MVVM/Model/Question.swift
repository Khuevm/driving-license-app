//
//  Question.swift
//  VNDrivingLicense
//
//  Created by DuNT on 25/03/2022.
//

import UIKit
import FMDB

class Question: BaseEntity, NSCopying {
    var id = 0
    var questionType_id = 0
    var questionText = ""
    var questionImageName = ""
    var answer_1 = ""
    var answer_2 = ""
    var answer_3 = ""
    var answer_4 = ""
    var answer_correct = 0
    var explanation = ""
    var question_die = 0 // 0:False - 1:True
    var practice_answer = 0
    
    var selectedAnswer: Int = 0
    
    override init() {
        super.init()
    }
    
    override init(resultSet: FMResultSet) {
        super.init(resultSet: resultSet)
        
        self.id = Int(resultSet.int(forColumn: "id"))
        self.questionType_id = Int(resultSet.int(forColumn: "questionType_id"))
        self.questionText = resultSet.string(forColumn: "questionText") ?? ""
        self.questionImageName = resultSet.string(forColumn: "questionImageName") ?? ""
        self.answer_1 = resultSet.string(forColumn: "answer_1") ?? ""
        self.answer_2 = resultSet.string(forColumn: "answer_2") ?? ""
        self.answer_3 = resultSet.string(forColumn: "answer_3") ?? ""
        self.answer_4 = resultSet.string(forColumn: "answer_4") ?? ""
        self.answer_correct = Int(resultSet.int(forColumn: "answerCorrect"))
        self.explanation = resultSet.string(forColumn: "explanation") ?? ""
        self.question_die = Int(resultSet.int(forColumn: "question_die"))
        self.practice_answer = Int(resultSet.int(forColumn: "practice_answer"))
    }
    
    func practicePass() -> Bool {
        return self.answer_correct == self.practice_answer
    }
    
    override func saveUpdate() {
        super.saveUpdate()
        let query = "update Question set practice_answer = \(self.practice_answer) where id = \(self.id)"
        _ = DBController.shared.excuteUpdate(query: query)
    }
    
    func questionImageURL() -> URL? {
        if questionImageName.isEmpty {
            return nil
        }
        
        return getURLFromPath(questionImageName)
    }
    
    private func getURLFromPath(_ path: String) -> URL? {
        if path.isEmpty {
            return nil
        }
        
        guard let lastDotIndex = path.lastIndex(of: ".") else {
            return nil
        }
        
        let beginExtIndex = path.index(lastDotIndex, offsetBy: 1)
        
        let name = String(path[path.startIndex..<lastDotIndex])
        let ext = String(path[beginExtIndex..<path.endIndex])
        
        return Bundle.main.url(forResource: name, withExtension: ext)
    }
    
    func copy(with zone: NSZone? = nil) -> Any {
        let copyObj = Question()
        copyObj.id = id
        copyObj.questionType_id = questionType_id
        copyObj.questionText = questionText
        copyObj.questionImageName = questionImageName
        copyObj.answer_1 = answer_1
        copyObj.answer_2 = answer_2
        copyObj.answer_3 = answer_3
        copyObj.answer_4 = answer_4
        copyObj.answer_correct = answer_correct
        copyObj.explanation = explanation
        copyObj.question_die = question_die
        copyObj.practice_answer = practice_answer
        return copyObj
    }
}
