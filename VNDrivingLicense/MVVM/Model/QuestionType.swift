//
//  QuestionType.swift
//  VNDrivingLicense
//
//  Created by DuNT on 25/03/2022.
//

import UIKit
import FMDB

class QuestionType: BaseEntity, NSCopying {
    var id = 0
    var title = ""
    var desc = ""
    var totalQuestion = 0
    var imageName = ""
    
    override init() {
        super.init()
    }

    override init(resultSet: FMResultSet) {
        super.init(resultSet: resultSet)
        
        self.id = Int(resultSet.int(forColumn: "id"))
        self.title = resultSet.string(forColumn: "name") ?? ""
        self.desc = resultSet.string(forColumn: "desc") ?? ""
        self.totalQuestion = Int(resultSet.int(forColumn: "totalQuestion"))
        self.imageName = resultSet.string(forColumn: "imageName") ?? ""
    }
    
    func copy(with zone: NSZone? = nil) -> Any {
        let copyObject = QuestionType()
        copyObject.id = id
        copyObject.title = title
        copyObject.desc = desc
        copyObject.imageName = imageName
        copyObject.totalQuestion = totalQuestion
        return copyObject
    }
}
