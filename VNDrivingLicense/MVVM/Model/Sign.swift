//
//  Sign.swift
//  VNDrivingLicense
//
//  Created by Khue on 17/04/2022.
//

import Foundation
import FMDB

class Sign: BaseEntity {
    var id = 0
    var groupID = 0
    var signCode = ""
    var title = ""
    var desc = ""
    var imageName = ""
    var subGroupName = ""
    
    override init() {
        super.init()
    }
    
    override init(resultSet: FMResultSet) {
        super.init(resultSet: resultSet)
        
        self.id = Int(resultSet.int(forColumn: "id"))
        self.groupID = Int(resultSet.int(forColumn: "groupID"))
        self.signCode = resultSet.string(forColumn: "signCode") ?? ""
        self.title = resultSet.string(forColumn: "title") ?? ""
        self.desc = resultSet.string(forColumn: "desc") ?? ""
        self.imageName = resultSet.string(forColumn: "imageName") ?? ""
    }
    
    func imageURL() -> URL? {
        let sub = self.imageName.components(separatedBy: ".")
        if sub.count == 2 {
            return Bundle.main.url(forResource: sub[0], withExtension: sub[1])
        }
        return nil
    }
}
