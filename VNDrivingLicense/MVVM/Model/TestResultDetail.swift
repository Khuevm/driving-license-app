//
//  TestResultDetail.swift
//  VNDrivingLicense
//
//  Created by Khue on 02/05/2022.
//

import UIKit
import FMDB

class TestResultDetail: BaseEntity {
    var testID: Int!
    var questionID: Int!
    var answerNumber: Int!
    var correctNumber: Int!
    
    var question: Question?
    
    override init() {
        super.init()
    }
    
    override init(resultSet: FMResultSet) {
        super.init(resultSet: resultSet)
        
        self.testID = Int(resultSet.int(forColumn: "testID"))
        self.questionID = Int(resultSet.int(forColumn: "questionID"))
        self.answerNumber = Int(resultSet.int(forColumn: "answerNumber"))
        self.correctNumber = Int(resultSet.int(forColumn: "correctNumber"))
    }
    
    override func saveUpdate() {
        super.saveUpdate()
        if DBController.shared.isExistRecord(query:"select 1 from TestResultDetail where testID = \(self.testID!) and questionID = \(self.questionID!) limit 1") {
            let query = "update TestResultDetail set answerNumber = \(self.answerNumber!) where testID = \(self.testID!) and questionID = \(self.questionID!)"
            _ = DBController.shared.excuteUpdate(query: query)
        } else {
            insertToDatabase()
        }
    }
    
    override func insertToDatabase() {
        let query = "insert into TestResultDetail (testID, questionID, answerNumber, correctNumber) values (\(self.testID!), \(questionID!), \(answerNumber!), \(correctNumber!))"
        _ = DBController.shared.excuteUpdate(query: query)
    }
    
    func isPassed() -> Bool {
        return answerNumber == correctNumber
    }
}
