//
//  SignViewModel.swift
//  VNDrivingLicense
//
//  Created by Khue on 17/04/2022.
//

import UIKit
import Combine

// MARK: - ?
class SignSubGroup: NSCopying {
    var subGroupName = ""
    var signs = [Sign]()
    
    func copy(with zone: NSZone? = nil) -> Any {
        let copyObject = SignSubGroup()
        copyObject.subGroupName = subGroupName
        copyObject.signs = signs
        return copyObject
    }
}

protocol SignViewModelFactory: BaseViewModelFactory {
    var signGroupsPassThroughSubject: PassthroughSubject<[SignGroup], Never>? {get set}
    var signsSubGroupsPassThroughSuject: PassthroughSubject<[SignSubGroup], Never>? {get set}
    func selectSignGroup(_ signGroup: SignGroup)
}

class SignViewModel: SignViewModelFactory {
    
    var signGroupsPassThroughSubject: PassthroughSubject<[SignGroup], Never>?
    var signsSubGroupsPassThroughSuject: PassthroughSubject<[SignSubGroup], Never>?
    
    init() {
        self.signGroupsPassThroughSubject = PassthroughSubject()
        signsSubGroupsPassThroughSuject = PassthroughSubject()
    }
    
    func selectSignGroup(_ signGroup: SignGroup) {
        guard let signs = signGroup.signs else {
            self.signsSubGroupsPassThroughSuject?.send([])
            return
        }
        
        var dict = [String: SignSubGroup]()
        signs.forEach { sign in
            if let subGroup = dict[sign.subGroupName] {
                subGroup.signs.append(sign)
            } else {
                let subGroup = SignSubGroup()
                subGroup.subGroupName = sign.subGroupName
                subGroup.signs.append(sign)
                dict[sign.subGroupName] = subGroup
            }
        }
        
        var groups = [SignSubGroup]()
        dict.keys.forEach { key in
            if let subGroup = dict[key] {
                groups.append(subGroup)
            }
        }
        
        self.signsSubGroupsPassThroughSuject?.send(groups)
    }
    
    func appear() {
        let groups = DBController.shared.getListSignGroup()
        signGroupsPassThroughSubject?.send(groups)
    }
}
