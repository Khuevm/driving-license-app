//
//  SignVC.swift
//  VNDrivingLicense
//
//  Created by Khue on 17/04/2022.
//

import UIKit

class SignVC: BaseViewController<SignViewModelFactory> {
    var coordinator: SignCoordinator!
    // MARK: - IBOutlet
    @IBOutlet weak var backButton: UIButton!
    @IBOutlet weak var headMenu: HeadMenu!
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var headerView: UIView!
    
    // MARK: - Variable
    var signGroups = [SignGroup]()
    var items = [String]()
    var filterTrafficSign: [SignSubGroup]?
    var index = 0
    
    var signSubGroups = [SignSubGroup]()
    
    // MARK: - Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        config()
        bindViewModel()
    }

    // MARK: - Config
    func config() {
        backButton.setTitle("", for: .normal)
        
        headerView.shadowColor = .black
        headerView.shadowRadius = 8
        headerView.shadowOpacity = 0.08
        headerView.shadowOffset = CGSize(width: 0, height: 4)
        
        configCollectionView()
        configHeadMenuView()
    }
    
    func configHeadMenuView() {
        headMenu.delegate = self
        headMenu.dataSource = self
        headMenu.collectionView.contentInset = UIEdgeInsets(top: 0, left: 16, bottom: 0, right: 16)
    }
    
    func configCollectionView() {
        collectionView.shadowColor = .black
        collectionView.shadowRadius = 12
        collectionView.shadowOffset = CGSize(width: 0, height: 0)
        collectionView.shadowOpacity = 0.08
        collectionView.clipsToBounds = false
        
        // MARK: - ?
        collectionView.contentInset = UIEdgeInsets(top: 8, left: 0, bottom: 10, right: 0)
        collectionView.delegate = self
        collectionView.dataSource = self
        collectionView.registerCell(type: TrafficSignCollectionViewCell.self)
    }
    
    // MARK: - ?
    func bindViewModel() {
        viewModel.signGroupsPassThroughSubject?.sink(receiveValue: { signGroups in
            if let group = signGroups.first {
                self.viewModel.selectSignGroup(group)
            }
            self.signGroups = signGroups
            for i in 0..<signGroups.count {
                self.items.append(signGroups[i].groupName)
            }
            self.headMenu.reloadData()
        }).store(in: &cancellables)
        
        viewModel.signsSubGroupsPassThroughSuject?.sink(receiveValue: { subGroups in
            self.signSubGroups = subGroups
            self.filterTrafficSign = subGroups
            self.collectionView.reloadData()
            self.collectionView.setContentOffset(.zero, animated: false)
        }).store(in: &cancellables)
        
        viewModel.appear()
    }
    
    // MARK: - IBAction

    @IBAction func backButtonDidTap(_ sender: Any) {
        self.coordinator.stop()
    }
    
    // MARK: - Helper
    private func updateSignsShadow() {
        headerView.shadowOpacity = collectionView.contentOffset.y > 0 ? 0.08 : 0
    }
}

// MARK: - UICollectionViewDataSource
extension SignVC: UICollectionViewDataSource {
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return filterTrafficSign?.count ?? 0
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return filterTrafficSign?[section].signs.count ?? 0

    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueCell(type: TrafficSignCollectionViewCell.self, indexPath: indexPath) else {
            return UICollectionViewCell()
        }
        
        if let group = self.filterTrafficSign?[indexPath.section] {
            cell.bindData(group.signs[indexPath.row])
        }
        
        return cell
    }
    
}

// MARK: - UICollectionViewDelegateFlowLayout
extension SignVC: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let width = (collectionView.frame.width - 20) / 2
        return CGSize.init(width: width, height: 210)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 20
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 20
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        updateSignsShadow()
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        view.endEditing(true)
    }
    
    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        view.endEditing(true)
    }
}

// MARK: - HeadMenuDelegate
extension SignVC: HeadMenuDelegate {
    func headMenu(_ headMenu: HeadMenu, didSelectItemAtIndex index: Int) {
        self.view.endEditing(true)
        viewModel.selectSignGroup(signGroups[index])
        collectionView.setContentOffset(.zero, animated: false)
    }
}

// MARK: - HeadMenuDataSource
extension SignVC: HeadMenuDataSource {
    func headMenuListItem(_ headMenu: HeadMenu) -> [String] {
        return items
    }
}
