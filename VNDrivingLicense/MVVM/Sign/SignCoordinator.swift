//
//  SignCoordinator.swift
//  VNDrivingLicense
//
//  Created by Khue on 17/04/2022.
//

import UIKit

class SignCoordinator: BaseCoordinator {
    var started: Bool = false
    weak var controller: SignVC?
    var navigation: UINavigationController
    
    init(navigation: UINavigationController) {
        self.navigation = navigation
        
    }
    
    func start() {
        if !started {
            started = true
            let controller = SignVC.factory()
            controller.coordinator = self
            
            navigation.pushViewController(controller, animated: true)
            // Show Controller
            self.controller = controller
        }
    }

    func stop(completion: (() -> Void)? = nil) {
        if started {
            started = false
            controller?.cancellables.removeAll()
            navigation.popViewController(animated: true)
        }
    }
}
