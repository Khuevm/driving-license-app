//
//  ExamOverviewViewModel.swift
//  VNDrivingLicense
//
//  Created by Khue on 02/05/2022.
//

import UIKit
import Combine

protocol ExamOverviewViewModelFactory: BaseViewModelFactory {
    var allTestPassThroughSubject: PassthroughSubject<[Test], Never>? {get set}
}

class ExamOverviewViewModel: ExamOverviewViewModelFactory {
    
    var allTestPassThroughSubject: PassthroughSubject<[Test], Never>?
    
    init() {
        allTestPassThroughSubject = PassthroughSubject()
    }
    
    func appear() {
        let tests = DBController.shared.getAllTest()
        allTestPassThroughSubject?.send(tests)
    }
}
