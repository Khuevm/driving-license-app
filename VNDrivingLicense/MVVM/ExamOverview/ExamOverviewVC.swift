//
//  ExamOverviewVC.swift
//  VNDrivingLicense
//
//  Created by Khue on 02/05/2022.
//

import UIKit

class ExamOverviewVC: BaseViewController<ExamOverviewViewModelFactory> {
    var coordinator: ExamOverviewCoordinator!
    // MARK: - IBOutlet
    @IBOutlet weak var backButton: UIButton!
    @IBOutlet weak var collectionView: UICollectionView!

    // MARK: - Varialbe
    private var tests: [Test]?

    // MARK: - Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        config()
        bindViewModel()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)

        viewModel.appear()
    }

    // MARK: - Config
    func config() {
        backButton.setTitle("", for: .normal)
        
        collectionView.dataSource = self
        collectionView.delegate = self
        collectionView.registerCell(type: ExamOverviewCell.self)
        collectionView.contentInset = UIEdgeInsets(top: 8, left: 27, bottom: 30, right: 28)
    }

    func bindViewModel() {
        viewModel.allTestPassThroughSubject?.sink(receiveValue: { tests in
            self.tests = tests
            self.collectionView.reloadData()
        }).store(in: &cancellables)
    }

    // MARK: - IBAction
    @IBAction func backButtonDidTap(_ sender: Any) {
        self.coordinator.stop()
    }


    // MARK: - Helper
}

extension ExamOverviewVC: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.tests?.count ?? 0
    }
//
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueCell(type: ExamOverviewCell.self, indexPath: indexPath), let test = self.tests?[indexPath.row] else { return UICollectionViewCell() }
        cell.bindData(test)
        return cell
    }
}

extension ExamOverviewVC: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 20
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 20
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let width = (collectionView.bounds.width - 75) / 2
        return CGSize(width: width, height: 150)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if let _ = tests?[indexPath.row].testResult {
            let coordinator = ExamResultCoordinator(presentingVC: self, test: tests![indexPath.row], testResult: nil, testResultDetails: nil, delegate: self)
            coordinator.start()
            return
        }
        
        if let navigation = self.navigationController, let test = self.tests?[indexPath.row]  {
            let coordinator = ExamCoordinator(navigation: navigation, test: test)
            coordinator.start()
        }
    }
}

extension ExamOverviewVC: ExamResultVCDelegate {
    func testResultVCDidTapClose(_ vc: ExamResultVC) {
        //
    }
    
    func testResultVCDidTapRetakeButton(test: Test) {
        let coordinator = ExamCoordinator(navigation: self.navigationController!, test: test)
        coordinator.start()
    }
}
