//
//  PracticeOverViewViewModel.swift
//  VNDrivingLicense
//
//  Created by Khue on 17/04/2022.
//

import UIKit
import Combine

protocol PracticeOverViewViewModelFactory: BaseViewModelFactory {
    var questionTypePassThroughSubject: PassthroughSubject<[QuestionType], Never>? {get set}
}

class PracticeOverViewViewModel: PracticeOverViewViewModelFactory {
    var questionTypePassThroughSubject: PassthroughSubject<[QuestionType], Never>?
    
    init(){
        self.questionTypePassThroughSubject = PassthroughSubject()
    }
    
    func appear() {
        let questionType = DBController.shared.getQuestionType()
        questionTypePassThroughSubject?.send(questionType)
    }
}
