//
//  PracticeOverViewVC.swift
//  VNDrivingLicense
//
//  Created by Khue on 17/04/2022.
//

import UIKit

class PracticeOverViewVC: BaseViewController<PracticeOverViewViewModelFactory> {
    var coordinator: PracticeOverViewCoordinator!
    // MARK: - IBOutlet
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var backButton: UIButton!
    
    // MARK: - Variables
    private var questionType = [QuestionType]()
    
    
    // MARK: - Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        config()
        bindViewModel()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        viewModel.appear()
    }

    // MARK: - Config
    func config() {
        configTableView()

    }
    
    func configTableView(){
        tableView.clipsToBounds = false
        tableView.shadowColor = .black
        tableView.shadowOpacity = 0.08
        tableView.shadowOffset = CGSize(width: 0, height: 2)
        tableView.shadowRadius = 8
        
        tableView.delegate = self
        tableView.dataSource = self
        tableView.separatorStyle = .none
        tableView.registerCell(type: PracticeOverviewTableViewCell.self)
        tableView.contentInset = UIEdgeInsets(top: 10, left: 0, bottom: 20, right: 0)
    }
    
    func bindViewModel() {
        viewModel.questionTypePassThroughSubject?.sink(receiveValue: { questionType in
            self.questionType = questionType
            self.tableView.reloadData()
        }).store(in: &cancellables)
     }
    
    // MARK: - IBAction

    @IBAction func backButtonDidTap(_ sender: Any) {
        self.coordinator.stop()
    }
    
    // MARK: - Helper
}

// MARK: - UITableViewDataSource
extension PracticeOverViewVC: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.questionType.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let  cell = tableView.dequeueCell(type: PracticeOverviewTableViewCell.self) else { return UITableViewCell() }
        cell.bindData(self.questionType[indexPath.row])
        
        return cell
    }
}

// MARK: - UITableViewDelegate
extension PracticeOverViewVC: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let navigation = self.navigationController {
            let coordinator = PracticeCoordinator(navigation: navigation, questionType: self.questionType[indexPath.row])
            coordinator.start()
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return tableView.estimatedRowHeight
    }
}
