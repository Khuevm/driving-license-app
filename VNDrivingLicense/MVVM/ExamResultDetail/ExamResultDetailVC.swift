//
//  ExamResultDetailVC.swift
//  VNDrivingLicense
//
//  Created by DuNT on 30/04/2022.
//

import UIKit

class ExamResultDetailVC: BaseViewController<ExamResultDetailViewModelFactory> {
    var coordinator: ExamResultDetailCoordinator!
    var testResultDetail: [TestResultDetail]?
    var selectQuestionIndex: Int?
    var testResult: TestResult?
    var test: Test?
    // MARK: - IBOutlet
    @IBOutlet weak var testNameLabel: UILabel!
    @IBOutlet weak var backButton: UIButton!
    @IBOutlet weak var headerView: UIView!
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var questionControlView: QuestionControlView!
    @IBOutlet weak var footerQuestionControlView: FooterQuestionControlView!
    
    // MARK: - Variable
    private var currentQuestionIndex = 0
    
    // MARK: - Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        config()
        bindViewModel()
    }
    
    override func viewDidFirstLayoutSubView() {
        super.viewDidFirstLayoutSubView()
        questionControlView.reloadData()
        collectionView.reloadData()
        self.selectedQuestion(index: self.currentQuestionIndex, animated: false)
    }

    // MARK: - Config
    func config() {
        testNameLabel.text = self.test?.testName ?? ""
        backButton.setTitle("", for: .normal)
        
        headerView.shadowColor = .black
        headerView.shadowOpacity = 0.08
        headerView.shadowOffset = CGSize(width: 0, height: 4)
        headerView.shadowRadius = 8
        
        configCollectionView()
        questionControlView.delegate = self
        questionControlView.dataSource = self
        
        footerQuestionControlView.shadowColor = .black
        footerQuestionControlView.shadowOpacity = 0.15
        footerQuestionControlView.shadowOffset = CGSize(width: 0, height: -1)
        footerQuestionControlView.shadowRadius = 8
        footerQuestionControlView.delegate = self
        
        currentQuestionIndex = selectQuestionIndex ?? 0
    }
    
    func configCollectionView() {
        collectionView.delegate = self
        collectionView.dataSource = self
        collectionView.isPagingEnabled = true
        collectionView.registerCell(type: AnswerQuestionCell.self)
    }
    
    func bindViewModel() {
        
    }
    
    // MARK: - IBAction
    @IBAction func backButtonDidTap(_ sender: Any) {
        coordinator.stop()
    }
    
    
    // MARK: - Helper
    func selectedQuestion(index: Int, animated: Bool) {
        currentQuestionIndex = index
        collectionView.setContentOffset(CGPoint.init(x: collectionView.frame.width * CGFloat(index), y: 0), animated: animated)
        questionControlView.selectQuestion(index: index, animated: animated)
        reloadFooterQuestionControlView()
    }
    
    func reloadFooterQuestionControlView() {
        self.footerQuestionControlView.isEnableNextButton = canNextQuestion()
        self.footerQuestionControlView.isEnablePreviousButton = canPresQuestion()
    }
    
    func canNextQuestion() -> Bool {
        return currentQuestionIndex < testResultDetail!.count - 1
    }
    
    func canPresQuestion() -> Bool {
        return currentQuestionIndex > 0
    }
}

// MARK: - UICollectionViewDataSource
extension ExamResultDetailVC: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.testResultDetail?.count ?? 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueCell(type: AnswerQuestionCell.self, indexPath: indexPath), let question = self.testResultDetail?[indexPath.row].question else { return UICollectionViewCell() }
        
        cell.setQuestion(question, questionIndex: indexPath.row)
        cell.showSelectAnswer()
        cell.showExamResult()
        return cell
    }
}

// MARK: - UICollectionViewDelegateFlowLayout
extension ExamResultDetailVC: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return collectionView.frame.size
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        finishedScroll(scrollView)
    }
    
    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        finishedScroll(scrollView)
    }
    
    private func finishedScroll(_ scrollView: UIScrollView) {
        currentQuestionIndex = Int(scrollView.contentOffset.x / scrollView.frame.width)
        questionControlView.selectQuestion(index: currentQuestionIndex, animated: true)
        reloadFooterQuestionControlView()
    }
}

// MARK: - QuestionControlViewDelegate
extension ExamResultDetailVC: QuestionControlViewDelegate {
    func questionControlView(_ view: QuestionControlView, didSelectQuestionIndex index: Int) {
        currentQuestionIndex = index
        view.selectQuestion(index: index, animated: true)
        view.reloadData()
        collectionView.scrollToItem(at: IndexPath.init(row: index, section: 0), at: .left, animated: false)
        reloadFooterQuestionControlView()
    }
}

// MARK: - QuestionControlViewDataSource
extension ExamResultDetailVC: QuestionControlViewDataSource {
    func questionControlViewNumberOfQuestion(_ view: QuestionControlView) -> Int {
        return self.testResultDetail?.count ?? 0
    }
    
    func questionControlView(_ view: QuestionControlView, textColorForItemAt index: Int) -> UIColor {
        return .white
    }
    
    func questionControlView(_ view: QuestionControlView, borderColorForItemAt index: Int) -> UIColor? {
        return index == currentQuestionIndex ? UIColor(rgb: 0xF2994A) : nil
    }
    
    func questionControlView(_ view: QuestionControlView, backgroundColorForItemAt index: Int) -> UIColor {
        if let question = testResultDetail?[index] {
            return question.isPassed() ? UIColor(named: "PassedColor")! : UIColor(named: "FailedColor")!
        }
        return UIColor(named: "FailedColor")!
    }
}

// MARK: - FooterQuestionControlViewDelegate
extension ExamResultDetailVC: FooterQuestionControlViewDelegate {
    func footerQuestionControlViewDidTapNextButton(_ view: FooterQuestionControlView) {
        self.selectedQuestion(index: currentQuestionIndex + 1, animated: true)
    }
    
    func footerQuestionControlViewDidTapPreviousButton(_ view: FooterQuestionControlView) {
        self.selectedQuestion(index: currentQuestionIndex - 1, animated: true)
    }
}
