//
//  SplashCoordinator.swift
//  VNDrivingLicense
//
//  Created by Khue on 26/03/2022.
//

import UIKit

class SplashCoordinator: BaseCoordinator {
    var started: Bool = false
    weak var controller: SplashVC?
    var window: UIWindow?
    
    init(window: UIWindow?) {
        self.window = window
        
    }
    
    func start() {
        if !started {
            started = true
            let controller = SplashVC.factory()
            controller.coordinator = self
            
            let nav = UINavigationController.init(rootViewController: controller)
            nav.isNavigationBarHidden = true
            window?.rootViewController = nav
            // Show Controller
            self.controller = controller
        }
    }

    func stop(completion: (() -> Void)? = nil) {
        if started {
            started = false
        }
    }
}
