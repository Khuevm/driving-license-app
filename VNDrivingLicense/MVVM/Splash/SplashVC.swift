//
//  SplashVC.swift
//  VNDrivingLicense
//
//  Created by Khue on 26/03/2022.
//

import UIKit

class SplashVC: BaseViewController<SplashViewModelFactory> {
    var coordinator: SplashCoordinator!
    // MARK: - IBOutlet
    
    @IBOutlet var titleLable: UILabel!
    
    
    // MARK: - Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        config()
        bindViewModel()
    }

    // MARK: - Config
    func config() {
        let attributeString = NSMutableAttributedString(string: "Bằng lái xe A1")
        attributeString.addAttribute(.foregroundColor, value: UIColor(rgb: 0xF2994A), range: NSRange(location: 0, length: attributeString.length))
        attributeString.addAttribute(.foregroundColor, value: UIColor(rgb: 0x212121), range: NSRange(location: 0, length: 11))
        titleLable.attributedText = attributeString

        DispatchQueue.main.asyncAfter(deadline: .now() + 3) {
            if let navigation = self.navigationController {
                let coordinator = HomeCoordinator(navigation: navigation)
                coordinator.start()
            }
        }
    }
    
    func bindViewModel() {
        
    }
    
    // MARK: - IBAction

    
    // MARK: - Helper
}
