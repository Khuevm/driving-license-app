//
//  SplashViewModel.swift
//  VNDrivingLicense
//
//  Created by Khue on 26/03/2022.
//

import UIKit

protocol SplashViewModelFactory: BaseViewModelFactory {
    
}

class SplashViewModel: SplashViewModelFactory {
    
    func appear() {
        
    }
}
