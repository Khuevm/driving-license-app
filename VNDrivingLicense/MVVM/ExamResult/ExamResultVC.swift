//
//  ExamResultVC.swift
//  VNDrivingLicense
//
//  Created by Khue on 02/05/2022.
//

import UIKit

protocol ExamResultVCDelegate: AnyObject {
    func testResultVCDidTapClose(_ vc: ExamResultVC)
    func testResultVCDidTapRetakeButton(test:Test)
}

class ExamResultVC: BaseViewController<ExamResultViewModelFactory> {
    var testResult: TestResult?
    var testResultDetails: [TestResultDetail]?
    var test: Test!
    weak var delegate: ExamResultVCDelegate?
    var coordinator: ExamResultCoordinator!
    // MARK: - IBOutlet
    @IBOutlet weak var backButton: UIButton!
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var subtitleLabel: UILabel!
    @IBOutlet weak var scoreLabel: UILabel!
    @IBOutlet weak var imageView: UIImageView!
    
    @IBOutlet weak var completeLabel: UILabel!
    @IBOutlet weak var correctLabel: UILabel!
    @IBOutlet weak var wrongLabel: UILabel!
    
    @IBOutlet weak var retakeButton: UIButton!
    @IBOutlet weak var reviewButton: UIButton!
    
    // MARK: - Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        config()
        bindViewModel()
        
    }
    
    override func viewDidFirstLayoutSubView() {
        super.viewDidFirstLayoutSubView()
        if testResult == nil {
            viewModel.appear()
        } else {
            self.bindDataToView()
        }
    }

    // MARK: - Config
    func config() {
        backButton.setTitle("", for: .normal)
    }
    
    func bindViewModel() {
        viewModel.test = test
        viewModel.testResultPassthroughSubject?.combineLatest(viewModel.testResultDetailsPassthroughSubject!).sink(receiveValue: { result in
            self.testResult = result.0
            self.testResultDetails = result.1
            self.bindDataToView()
        }).store(in: &cancellables)
        
    }
    
    // MARK: - IBAction
    @IBAction func backButtonDidTap(_ sender: Any) {
        coordinator.stop()
        delegate?.testResultVCDidTapClose(self)
    }
    
    @IBAction func retakeButtonDidTap(_ sender: Any) {
        coordinator.stop()
        delegate?.testResultVCDidTapRetakeButton(test: self.test)
    }
    
    @IBAction func reviewButtonDidTap(_ sender: Any) {
        if let navigation = self.navigationController, let testResult = testResult, let testResultDetails = testResultDetails {
            let coordinator = ExamResultDetailCoordinator(navigation: navigation, test: test, testResult: testResult, testResultDetails: testResultDetails, selectQuestionIndex: 0)
            coordinator.start()
        }
    }
    // MARK: - Helper
    
    private func bindDataToView() {
        scoreLabel.text = "\(testResult?.numberAnswerCorrect ?? 0)/\(testResult?.totalQuestion ?? 0)"
        completeLabel.text = "\(100 - Int(Double(testResult?.numberAnswerRemaining ?? 0) / Double(testResult?.totalQuestion ?? 0) * 100))%"
        correctLabel.text = "\(testResult?.numberAnswerCorrect ?? 0)"
        wrongLabel.text = "\(testResult?.numberAnswerIncorrect ?? 0)"
        if testResult!.isPassed() {
            titleLabel.text = "Chúc mừng"
            subtitleLabel.text = "Bạn đã vượt qua bài thi 😎😎\nHãy tiếp tục phát huy"
            imageView.image = UIImage(named: "ic_pass")!
            scoreLabel.textColor = UIColor(named: "PassedColor")!
        }else {
            titleLabel.text = "Bạn đã trượt bài thi"
            subtitleLabel.text = "Hãy luyện tập thêm cho lần thi sau 👏🏼👏🏼"
            imageView.image = UIImage(named: "ic_fail")!
            scoreLabel.textColor = UIColor(named: "FailedColor")!
        }
        self.view.layoutIfNeeded()
    }
    
}
