//
//  ExamViewModel.swift
//  VNDrivingLicense
//
//  Created by DuNT on 27/04/2022.
//

import UIKit
import Combine

protocol ExamViewModelFactory: BaseViewModelFactory {
    var test: Test! { get set }
    var questionsPassthroughSubject: PassthroughSubject<[Question], Never>? {get set}
    func submitExam(testID: Int, questions: [Question], selectedAnswers: [Int]) -> (testResult: TestResult, testResultDetails: [TestResultDetail])
}

class ExamViewModel: ExamViewModelFactory {
    
    var test: Test!
    var questionsPassthroughSubject: PassthroughSubject<[Question], Never>?
    
    init() {
        questionsPassthroughSubject = PassthroughSubject()
    }
    
    func appear() {
        let questions = DBController.shared.getListQuestion(of: self.test)
        questionsPassthroughSubject?.send(questions)
    }
    
    func submitExam(testID: Int, questions: [Question], selectedAnswers: [Int]) -> (testResult: TestResult, testResultDetails: [TestResultDetail]) {
        var numberAnswerCorrect = 0
        var numberAnswerInCorrect = 0
        var numberAnswerRemaining = 0
        var isFailDieQuestion = 0
        for i in 0..<selectedAnswers.count {
            if selectedAnswers[i] == questions[i].answer_correct {
                numberAnswerCorrect += 1
            } else {
                numberAnswerInCorrect += 1
                if selectedAnswers[i] == 0 {
                    numberAnswerRemaining += 1
                }
                if questions[i].question_die == 1 {
                    isFailDieQuestion = 1
                }
            }
        }
        
        let testResult = TestResult()
        testResult.testID = testID
        testResult.miniumAnswerCorrect = 21
        testResult.numberAnswerCorrect = numberAnswerCorrect
        testResult.numberAnswerIncorrect = numberAnswerInCorrect
        testResult.numberAnswerRemaining = numberAnswerRemaining
        testResult.isFailDieQuestion = isFailDieQuestion
        testResult.totalQuestion = questions.count
        testResult.saveUpdate()
        
        testResult.testResultDetails = [TestResultDetail]()
        
        for i in 0..<selectedAnswers.count {
            let testResultDetail = TestResultDetail()
            testResultDetail.testID = testID
            testResultDetail.questionID = questions[i].id
            testResultDetail.answerNumber = selectedAnswers[i]
            testResultDetail.correctNumber = questions[i].answer_correct
            testResultDetail.saveUpdate()
            testResultDetail.question = questions[i]
            testResult.testResultDetails?.append(testResultDetail)
        }
        
        return (testResult: testResult, testResultDetails: testResult.testResultDetails!)
    }
}
