//
//  ExamCoordinator.swift
//  VNDrivingLicense
//
//  Created by DuNT on 27/04/2022.
//

import UIKit

class ExamCoordinator: BaseCoordinator {
    var started: Bool = false
    weak var controller: ExamVC?
    var navigation: UINavigationController
    var test: Test
    
    init(navigation: UINavigationController, test: Test) {
        self.navigation = navigation
        self.test = test
    }
    
    func start() {
        if !started {
            started = true
            let controller = ExamVC.factory()
            controller.coordinator = self
            controller.test = test
            
            navigation.pushViewController(controller, animated: true)
            // Show Controller
            self.controller = controller
        }
    }

    func stop(completion: (() -> Void)? = nil) {
        if started {
            started = false
            controller?.cancellables.removeAll()
            navigation.popViewController(animated: true)
        }
    }
}
