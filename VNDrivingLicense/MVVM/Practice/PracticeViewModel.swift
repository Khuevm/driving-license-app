//
//  PracticeViewModel.swift
//  VNDrivingLicense
//
//  Created by Khue on 01/05/2022.
//


import UIKit
import Combine

protocol PracticeViewModelFactory: BaseViewModelFactory {
    var questionType: QuestionType! {get set}
    var questionsPassthroughSubject: PassthroughSubject <[Question], Never>? {get set}
    func savePracticeHistory(_ question: Question)
}

class PracticeViewModel: PracticeViewModelFactory {
    
    var questionType: QuestionType!
    var questionsPassthroughSubject: PassthroughSubject <[Question], Never>?
    
    init() {
        questionsPassthroughSubject = PassthroughSubject()
    }
    
    func appear() {
        let questions =  DBController.shared.getListQuestion(questionTypeID: questionType.id)
        questionsPassthroughSubject?.send(questions)
    }
    
    func savePracticeHistory(_ question: Question) {
        question.saveUpdate()
    }
}

