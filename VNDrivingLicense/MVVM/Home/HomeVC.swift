//
//  HomeVC.swift
//  VNDrivingLicense
//
//  Created by Khue on 26/03/2022.
//

import UIKit

class HomeVC: BaseViewController<HomeViewModelFactory> {
    var coordinator: HomeCoordinator!
    // MARK: - IBOutlet
    @IBOutlet weak var headerView: UIView!
    @IBOutlet weak var lawButton: UIButton!
    @IBOutlet weak var signButton: UIButton!
    @IBOutlet weak var practiceView: UIView!
    @IBOutlet weak var testView: UIView!
    
    //Practice
    @IBOutlet weak var totalQuestionLabel: UILabel!
    @IBOutlet weak var numberRemainingLabel: UILabel!
    @IBOutlet weak var numberQuestionWrongLabel: UILabel!
    @IBOutlet weak var numberQuestionCorrectLabel: UILabel!
    @IBOutlet weak var pieChart: PieChart!
    
    @IBOutlet weak var scoreLabel: UILabel!
    @IBOutlet weak var numberTestDoneLabel: UILabel!
    @IBOutlet weak var progressGradientView: ProgressGradientView!
    // MARK: - Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        config()
        bindViewModel()
    }
    
    override func viewDidFirstLayoutSubView() {
        super.viewDidFirstLayoutSubView()
        self.view.layoutIfNeeded()
        headerView.setupCornerRadius(topLeftRadius: 0, topRightRadius: 0, bottomLeftRadius: 20, bottomRightRadius: 20)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        viewModel.appear()
        view.layoutIfNeeded()
    }

    // MARK: - Config
    func config() {
        lawButton.shadowOffset = CGSize(width: 0, height: 0)
        lawButton.shadowColor = UIColor(rgb: 0x000000)
        lawButton.shadowOpacity = 0.1
        lawButton.shadowRadius = 12
        lawButton.cornerRadius = 8
        lawButton.clipsToBounds = false
        let lawAttributeString = NSMutableAttributedString(string: "Mức phạt")
        lawAttributeString.addAttribute(.font, value: UIFont.systemFont(ofSize: 16, weight: .semibold), range: NSRange(location: 0, length: lawAttributeString.length))
        lawButton.setAttributedTitle(lawAttributeString, for: .normal)
        
        signButton.shadowOffset = CGSize(width: 0, height: 0)
        signButton.shadowColor = UIColor(rgb: 0x000000)
        signButton.shadowOpacity = 0.1
        signButton.shadowRadius = 12
        signButton.cornerRadius = 8
        signButton.clipsToBounds = false
        let signAttributeString = NSMutableAttributedString(string: "Biển báo")
        signAttributeString.addAttribute(.font, value: UIFont.systemFont(ofSize: 16, weight: .semibold), range: NSRange(location: 0, length: signAttributeString.length))
        signButton.setAttributedTitle(signAttributeString, for: .normal)
        
        practiceView.shadowOffset = CGSize(width: 0, height: 0)
        practiceView.shadowColor = UIColor(rgb: 0x000000)
        practiceView.shadowOpacity = 0.1
        practiceView.shadowRadius = 12
        practiceView.cornerRadius = 12
        practiceView.clipsToBounds = false
        
        testView.shadowOffset = CGSize(width: 0, height: 0)
        testView.shadowColor = UIColor(rgb: 0x000000)
        testView.shadowOpacity = 0.1
        testView.shadowRadius = 12
        testView.cornerRadius = 12
        testView.clipsToBounds = false
        
        progressGradientView.gradientColors = [UIColor(rgb: 0xFFD4AE).cgColor, UIColor(rgb: 0xF2994A).cgColor]

    }
    
    func bindViewModel() {
        viewModel.allQuestionPassThroughSubject?.sink(receiveValue: { questions in
            let attributed1 = NSMutableAttributedString(string: "\(questions.count)\n")
            attributed1.addAttribute(.font, value: UIFont.systemFont(ofSize: 17, weight: .semibold), range: NSRange(location: 0, length: attributed1.length))

            let attributed2 = NSMutableAttributedString(string: "Câu hỏi")
            attributed2.addAttribute(.font, value: UIFont.systemFont(ofSize: 12, weight: .medium), range: NSRange(location: 0, length: attributed2.length))

            attributed1.append(attributed2)
            self.totalQuestionLabel.attributedText = attributed1

            let numberCorrect = questions.filter{($0.practicePass())}.count
            let numberIncorrect = questions.filter{(!($0.practicePass()) && $0.practice_answer != 0)}.count
            let numberRemaining = questions.filter{($0.practice_answer == 0)}.count

            self.numberQuestionCorrectLabel.text = "\(numberCorrect)"
            self.numberQuestionWrongLabel.text = "\(numberIncorrect)"
            self.numberRemainingLabel.text = "\(numberRemaining)"

            self.pieChart.correctProgress = CGFloat(numberCorrect) / CGFloat(questions.count)
            self.pieChart.wrongProgress = CGFloat(numberIncorrect) / CGFloat(questions.count)
            self.pieChart.correctColor = UIColor(named: "PassedColor")!
            self.pieChart.wrongColor = UIColor(named: "FailedColor")!
        }).store(in: &cancellables)
        
        viewModel.allTestPassThroughSubject?.sink(receiveValue: { tests in
            let numberTestDone = tests.filter({$0.testResult != nil}).count
            let totalTest = tests.count
            let totalScore = 25
            var totalScoreDone = 0
            tests.filter({$0.testResult != nil}).forEach { test in
                totalScoreDone += test.testResult!.numberAnswerCorrect
            }
            let averageScore = numberTestDone != 0 ? Double(totalScoreDone)/Double(numberTestDone) : 0.0
            
            self.numberTestDoneLabel.text = "\(numberTestDone)/\(totalTest)"
            self.scoreLabel.text = "\(String(format: "%.1f", averageScore))/\(totalScore)"
            self.progressGradientView.progress = CGFloat(numberTestDone) / CGFloat(totalTest)
        }).store(in: &cancellables)
    }
    
    // MARK: - IBAction
    @IBAction func practiceButtonDidTap(_ sender: Any) {
        if let navigation = self.navigationController {
            let coordinator = PracticeOverViewCoordinator(navigation: navigation)
            coordinator.start()
        }
    }
    
    @IBAction func examButtonDidTap(_ sender: Any) {
        if let navigation = self.navigationController {
            let coordinator = ExamOverviewCoordinator(navigation: navigation)
            coordinator.start()
        }
    }
    
    @IBAction func signButtonDidTap(_ sender: Any) {
        if let navigation = self.navigationController {
            let coordinator = SignCoordinator(navigation: navigation)
            coordinator.start()
        }
    }
    
    @IBAction func lawButtonDidTap(_ sender: Any) {
        guard let path = Bundle.main.url(forResource: "Muc_phat_xe_may", withExtension: "pdf") else { return }
        let documentInteractionController = UIDocumentInteractionController.init(url: path)
        documentInteractionController.delegate = self
        documentInteractionController.presentPreview(animated: true)
    }
    // MARK: - Helper
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
}

extension HomeVC: UIDocumentInteractionControllerDelegate {
    func documentInteractionControllerViewControllerForPreview(_ controller: UIDocumentInteractionController) -> UIViewController {
        return self
    }
}
