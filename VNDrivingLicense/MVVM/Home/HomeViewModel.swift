//
//  HomeViewModel.swift
//  VNDrivingLicense
//
//  Created by Khue on 26/03/2022.
//

import UIKit
import Combine

protocol HomeViewModelFactory: BaseViewModelFactory {
    var allQuestionPassThroughSubject: PassthroughSubject<[Question], Never>? {get set}
    var allTestPassThroughSubject: PassthroughSubject<[Test], Never>? {get set}
    
}

class HomeViewModel: HomeViewModelFactory {
    var allQuestionPassThroughSubject: PassthroughSubject<[Question], Never>?
    var allTestPassThroughSubject: PassthroughSubject<[Test], Never>?
    
    init() {
        allQuestionPassThroughSubject = PassthroughSubject()
        allTestPassThroughSubject = PassthroughSubject()
    }
    
    func appear() {
        allQuestionPassThroughSubject?.send(DBController.shared.getAllQuestion())
        allTestPassThroughSubject?.send(DBController.shared.getAllTest())
    }
}
