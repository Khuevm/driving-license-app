//
//  IntroVC.swift
//  VNDrivingLicense
//
//  Created by Khue on 26/03/2022.
//

import UIKit

class IntroVC: BaseViewController<IntroViewModelFactory> {
    var coordinator: IntroCoordinator!
    // MARK: - IBOutlet
    @IBOutlet weak var titleLable: UILabel!
    @IBOutlet weak var startButton: UIButton!
    @IBOutlet var startButtonTopConstraint: NSLayoutConstraint!
    
    
    // MARK: - Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        config()
        bindViewModel()
    }

    // MARK: - Config
    func config() {
        let attributeString = NSMutableAttributedString(string: "Bằng lái xe A1")
        attributeString.addAttribute(.foregroundColor, value: UIColor(rgb: 0xF2994A), range: NSRange(location: 0, length: attributeString.length))
        attributeString.addAttribute(.foregroundColor, value: UIColor(rgb: 0x212121), range: NSRange(location: 0, length: 11))
        titleLable.attributedText = attributeString
        
        let startAttributeString = NSMutableAttributedString(string: "BẮT ĐẦU")
        startAttributeString.addAttribute(.font, value: UIFont.boldSystemFont(ofSize: 20), range: NSRange(location: 0, length: startAttributeString.length))
        startButton.setAttributedTitle(startAttributeString, for: .normal)
        
        startButtonTopConstraint.constant = UIScreen.main.bounds.height > 670 ? 90 : 40

    }
    
    func bindViewModel() {
        
    }
    
    // MARK: - IBAction
    @IBAction func startButtonDidTap(_ sender: Any) {
        if let navigation = self.navigationController {
            let coordinator = HomeCoordinator(navigation: navigation)
            coordinator.start()
        }
        
    }
    
    
    // MARK: - Helper
}
