//
//  QuestionControlView.swift
//  dvsa
//
//  Created by Viet Le on 08/02/2022.
//

import Foundation
import UIKit

protocol QuestionControlViewDataSource: AnyObject {
    
    func questionControlViewNumberOfQuestion(_ view: QuestionControlView) -> Int
    func questionControlView(_ view: QuestionControlView, textColorForItemAt index: Int) -> UIColor
    func questionControlView(_ view: QuestionControlView, borderColorForItemAt index: Int) -> UIColor?
    func questionControlView(_ view: QuestionControlView, backgroundColorForItemAt index: Int) -> UIColor
}

protocol QuestionControlViewDelegate: AnyObject {
    func questionControlView(_ view: QuestionControlView, didSelectQuestionIndex index: Int)
}

class QuestionControlView: UIView {
    weak var dataSource: QuestionControlViewDataSource?
    weak var delegate: QuestionControlViewDelegate?
    var newContentOffsetX: CGFloat = 0
    func reloadData() {
        numberOfItem = dataSource?.questionControlViewNumberOfQuestion(self) ?? 0
        collectionView.reloadData()
    }
    
    func reloadItem(at index: Int) {
        collectionView.reloadItems(at: [IndexPath.init(row: index, section: 0)])
    }
    
    func selectQuestion(index: Int, animated: Bool) {
        collectionView.reloadData()
        collectionView.scrollToItem(at: IndexPath(row: index, section: 0), at: .centeredHorizontally, animated: animated)
    }
    
    // MARK: - Variables
    private var collectionView: UICollectionView!
    private var numberOfItem = 0
    // MARK: - LifeCycle
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        customInit()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        customInit()
    }
    
    private func customInit() {
        let flowLayout = UICollectionViewFlowLayout.init()
        flowLayout.scrollDirection = .horizontal
        collectionView = UICollectionView.init(frame: .zero, collectionViewLayout: flowLayout)
        addSubview(collectionView)
        collectionView.translatesAutoresizingMaskIntoConstraints = false
        collectionView.fitSuperviewConstraint()
        collectionView.contentInset = UIEdgeInsets.init(top: 0, left: 20, bottom: 0, right: 20)
        collectionView.showsHorizontalScrollIndicator = false
        collectionView.registerCell(type: QuestionControllViewCell.self)
        collectionView.dataSource = self
        collectionView.delegate = self
        collectionView.backgroundColor = .clear
        
       // newContentOffsetX = (self.collectionView.contentSize.width/2) - (self.bounds.size.width/2)
    }
    // MARK: - Helper
    
}

extension QuestionControlView: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return numberOfItem
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueCell(type: QuestionControllViewCell.self, indexPath: indexPath) else {
            return UICollectionViewCell()
        }
        
        cell.titleLabel.text = "\(indexPath.row + 1)"
        cell.titleLabel.textColor = dataSource?.questionControlView(self, textColorForItemAt: indexPath.row) ?? .white
        cell.titleLabel.backgroundColor = dataSource?.questionControlView(self, backgroundColorForItemAt: indexPath.row) ?? .white
        
        if let borderColor = dataSource?.questionControlView(self, borderColorForItemAt: indexPath.row) {
            cell.titleLabel.borderWidth = 2
            cell.titleLabel.borderColor = borderColor
        } else {
            cell.titleLabel.borderWidth = 0
        }
        
        return cell
    }
}

extension QuestionControlView: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        delegate?.questionControlView(self, didSelectQuestionIndex: indexPath.row)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize.init(width: 30, height: 30)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 20
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 20
    }
}
