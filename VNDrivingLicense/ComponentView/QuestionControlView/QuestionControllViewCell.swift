//
//  QuestionControllViewCell.swift
//  dvsa
//
//  Created by Viet Le on 08/02/2022.
//

import UIKit

class QuestionControllViewCell: UICollectionViewCell {

    @IBOutlet weak var titleLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        titleLabel.cornerRadius = 15
        // Initialization code
    }

}
