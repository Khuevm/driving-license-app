//
//  AnswerQuestionCell.swift
//  VNDrivingLicense
//
//  Created by Khue on 01/05/2022.
//

import UIKit
import SDWebImage

class AnswerQuestionCell: UICollectionViewCell, AnswerCell {
    weak var delegate: AnswerCellDelegate?
    var question: Question?
    
    // MARK: - IBOutlet
    @IBOutlet weak var questionDieLabel: UILabel!
    @IBOutlet weak var numberQuestionLabel: UILabel!
    @IBOutlet weak var questionLabel: UILabel!
    
    @IBOutlet weak var questionImageView: UIImageView!
    
    @IBOutlet weak var answer1Label: UILabel!
    @IBOutlet weak var answer2Label: UILabel!
    @IBOutlet weak var answer3Label: UILabel!
    @IBOutlet weak var answer4Label: UILabel!
    @IBOutlet weak var explainLabel: UILabel!
    
    @IBOutlet weak var answer1View: DimableView!
    @IBOutlet weak var answer2View: DimableView!
    @IBOutlet weak var answer3View: DimableView!
    @IBOutlet weak var answer4View: DimableView!
    @IBOutlet weak var explainView: UIView!
    
    var canAnswer = false
    var tapDelayTime: TimeInterval = 0
    
    // MARK: - LifeCycle
    override func awakeFromNib() {
        super.awakeFromNib()
        reloadUI()
    }
    
    // MARK: - Public
    func setQuestion(_ question: Question, questionIndex: Int, canAnswer: Bool = true, hideQuestionDie: Bool = false) {
        self.question = question
        self.reloadUI()

        self.questionImageView.sd_setImage(with: question.questionImageURL())
        questionImageView.isHidden = question.questionImageName.isEmpty
        
        self.canAnswer = canAnswer

        numberQuestionLabel.text = "Câu hỏi \(questionIndex + 1)"
        questionLabel.text = question.questionText
        questionDieLabel.isHidden = question.question_die == 0 ||  hideQuestionDie
        answer1Label.text = question.answer_1.isEmpty ? "" : "A. " + question.answer_1
        answer2Label.text = question.answer_2.isEmpty ? "" : "B. " + question.answer_2
        answer3Label.text = question.answer_3.isEmpty ? "" : "C. " + question.answer_3
        answer4Label.text = question.answer_4.isEmpty ? "" : "D. " + question.answer_4

        self.answer4View.isHidden = question.answer_4.isEmpty
        self.answer3View.isHidden = question.answer_3.isEmpty

        self.layoutIfNeeded()
    }
    
    func reload() {
        reloadUI()
    }
    
    func showSelectAnswer() {
        reloadUI()
        let selectedAnswer = question?.selectedAnswer ?? 0
        switch selectedAnswer {
        case 1:
            answer1View.backgroundColor = UIColor.init(rgb: 0x5286F0)
            answer1View.borderWidth = 0
            answer1Label.textColor = .white
        case 2:
            answer2View.backgroundColor = UIColor.init(rgb: 0x5286F0)
            answer2View.borderWidth = 0
            answer2Label.textColor = .white
        case 3:
            answer3View.backgroundColor = UIColor.init(rgb: 0x5286F0)
            answer3View.borderWidth = 0
            answer3Label.textColor = .white
        case 4:
            answer4View.backgroundColor = UIColor.init(rgb: 0x5286F0)
            answer4View.borderWidth = 0
            answer4Label.textColor = .white
        default:
            break
        }
    }
    
    func showResultPractice(afterTime: TimeInterval = 0) {
        self.reloadUI()

        guard let question = question else {
            return
        }

        switch question.practice_answer {
        case 1:
            self.answer1View.backgroundColor = UIColor.init(rgb: 0xD3DAE7)
            self.answer1View.borderWidth = 0
        case 2:
            self.answer2View.backgroundColor = UIColor.init(rgb: 0xD3DAE7)
            self.answer2View.borderWidth = 0
        case 3:
            self.answer3View.backgroundColor = UIColor.init(rgb: 0xD3DAE7)
            self.answer3View.borderWidth = 0
        case 4:
            self.answer4View.backgroundColor = UIColor.init(rgb: 0xD3DAE7)
            self.answer4View.borderWidth = 0
        default:
            break
        }

        DispatchQueue.main.asyncAfter(deadline: .now() + afterTime) {
            switch question.practice_answer {
            case 1:
                self.answer1Label.textColor = .white
                self.answer1View.backgroundColor = UIColor(named: "FailedColor")
                self.answer1View.borderWidth = 0
            case 2:
                self.answer2Label.textColor = .white
                self.answer2View.backgroundColor = UIColor(named: "FailedColor")
                self.answer2View.borderWidth = 0
            case 3:
                self.answer3Label.textColor = .white
                self.answer3View.backgroundColor = UIColor(named: "FailedColor")
                self.answer3View.borderWidth = 0
            case 4:
                self.answer4Label.textColor = .white
                self.answer4View.backgroundColor = UIColor(named: "FailedColor")
                self.answer4View.borderWidth = 0
            default:
                break
            }

            switch  question.answer_correct {
            case 1:
                self.answer1Label.textColor = .white
                self.answer1View.backgroundColor = UIColor(named: "PassedColor")
                self.answer1View.borderWidth = 0

            case 2:
                self.answer2Label.textColor = .white
                self.answer2View.backgroundColor = UIColor(named: "PassedColor")
                self.answer2View.borderWidth = 0

            case 3:
                self.answer3Label.textColor = .white
                self.answer3View.backgroundColor = UIColor(named: "PassedColor")
                self.answer3View.borderWidth = 0

                
            case 4:
                self.answer4Label.textColor = .white
                self.answer4View.backgroundColor = UIColor(named: "PassedColor")
                self.answer4View.borderWidth = 0

            default:
                break
            }
            
            UIView.animate(withDuration: afterTime == 0 ? 0 : 0.3) {
                self.explainLabel.text = question.explanation.trim()
                self.explainView.isHidden = question.explanation.trim().isEmpty
            }
        }
    }
    
    func showExamResult() {
        self.reloadUI()

        guard let question = question else {
            return
        }

        switch question.selectedAnswer {
        case 1:
            self.answer1View.backgroundColor = UIColor.init(rgb: 0xFF4E4E)
            self.answer1View.borderWidth = 0
            self.answer1Label.textColor = .white
        case 2:
            self.answer2View.backgroundColor = UIColor.init(rgb: 0xFF4E4E)
            self.answer2View.borderWidth = 0
            self.answer2Label.textColor = .white
        case 3:
            self.answer3View.backgroundColor = UIColor.init(rgb: 0xFF4E4E)
            self.answer3View.borderWidth = 0
            self.answer3Label.textColor = .white
        case 4:
            self.answer4View.backgroundColor = UIColor.init(rgb: 0xFF4E4E)
            self.answer4View.borderWidth = 0
            self.answer4Label.textColor = .white
        default:
            break
        }

        switch  question.answer_correct {
        case 1:
            self.answer1Label.textColor = .white
            self.answer1View.backgroundColor = UIColor(named: "PassedColor")
            self.answer1View.borderWidth = 0

        case 2:
            self.answer2Label.textColor = .white
            self.answer2View.backgroundColor = UIColor(named: "PassedColor")
            self.answer2View.borderWidth = 0

        case 3:
            self.answer3Label.textColor = .white
            self.answer3View.backgroundColor = UIColor(named: "PassedColor")
            self.answer3View.borderWidth = 0

            
        case 4:
            self.answer4Label.textColor = .white
            self.answer4View.backgroundColor = UIColor(named: "PassedColor")
            self.answer4View.borderWidth = 0

        default:
            break
        }
        
        self.explainLabel.text = question.explanation.trim()
        self.explainView.isHidden = question.explanation.trim().isEmpty
        
    }
    
    func currentQuestion() -> Question? {
        return self.question
    }
    
    // MARK: - IBAction
    @IBAction func answer1ButtonDidTap(_ sender: Any) {
        if !canAnswer {
            return
        }
        self.delegate?.answerCell(self, selectAnswer: 1)
    }
    @IBAction func answer2ButtonDidTap(_ sender: Any) {
        if !canAnswer {
            return
        }
        self.delegate?.answerCell(self, selectAnswer: 2)
    }
    @IBAction func answer3ButtonDidTap(_ sender: Any) {
        if !canAnswer {
            return
        }
        self.delegate?.answerCell(self, selectAnswer: 3)
    }
    @IBAction func answer4ButtonDidTap(_ sender: Any) {
        if !canAnswer {
            return
        }
        self.delegate?.answerCell(self, selectAnswer: 4)
    }
    
    
    
    // MARK: - Helper
    private func reloadUI() {
        answer1View.backgroundColor = UIColor.init(rgb: 0xFFFFFF)
        answer1View.borderWidth = 1
        answer1View.borderColor = UIColor(rgb: 0xBAC4D7)
        answer1Label.textColor = UIColor.init(rgb: 0x212121)

        answer2View.backgroundColor = UIColor.init(rgb: 0xFFFFFF)
        answer2View.borderWidth = 1
        answer2View.borderColor = UIColor(rgb: 0xBAC4D7)
        answer2Label.textColor = UIColor.init(rgb: 0x212121)

        answer3View.backgroundColor = UIColor.init(rgb: 0xFFFFFF)
        answer3View.borderWidth = 1
        answer3View.borderColor = UIColor(rgb: 0xBAC4D7)
        answer3Label.textColor = UIColor.init(rgb: 0x212121)

        answer4View.backgroundColor = UIColor.init(rgb: 0xFFFFFF)
        answer4View.borderWidth = 1
        answer4View.borderColor = UIColor(rgb: 0xBAC4D7)
        answer4Label.textColor = UIColor.init(rgb: 0x212121)

        reloadLayout()
    }

    private func reloadLayout() {
        guard let question = question else {
            return
        }
        explainView.isHidden = true
        answer3View.isHidden = question.answer_3.isEmpty
        answer4View.isHidden = question.answer_4.isEmpty
        layoutIfNeeded()
    }

    private func getHeighForText(_ text: String, on label: UILabel) -> CGFloat {
        let testLabel = UILabel.init()
        testLabel.frame = CGRect.init(x: 0, y: 0, width: label.frame.width, height: 1000)
        testLabel.numberOfLines = 0
        testLabel.text = text
        testLabel.font = label.font
        testLabel.sizeToFit()
        return testLabel.frame.height
    }

}
