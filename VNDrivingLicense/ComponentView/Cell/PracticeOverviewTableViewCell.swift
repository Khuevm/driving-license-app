//
//  PracticeOverviewTableViewCell.swift
//  VNDrivingLicense
//
//  Created by Khue on 21/04/2022.
//

import UIKit

class PracticeOverviewTableViewCell: UITableViewCell {
    @IBOutlet weak var descLabel: UILabel!
    @IBOutlet weak var imgView: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.selectionStyle = .none
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func bindData(_ questionType: QuestionType) {
        self.nameLabel.text = questionType.title
        self.descLabel.text = questionType.desc
        self.imgView.image = UIImage(named: "\(questionType.imageName)")
    }
}
