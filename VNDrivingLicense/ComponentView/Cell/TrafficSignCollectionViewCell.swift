//
//  TrafficSignCollectionViewCell.swift
//  VNDrivingLicense
//
//  Created by Khue on 17/04/2022.
//

import UIKit
import SDWebImage

class TrafficSignCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var titleLable: UILabel!
    @IBOutlet weak var imageView: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        self.cornerRadius = 16
    }
    
    func bindData(_ sign: Sign){
        self.imageView.sd_setImage(with: sign.imageURL())
        self.titleLable.text = sign.title
    }

}
