//
//  ExamOverviewCell.swift
//  VNDrivingLicense
//
//  Created by Khue on 02/05/2022.
//

import UIKit

class ExamOverviewCell: UICollectionViewCell {

    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var gradeLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        clipsToBounds = false
        
        shadowColor = .black
        shadowRadius = 12
        shadowOpacity = 0.04
        shadowOffset = CGSize(width: 0, height: 0)
    }
    
    func bindData(_ test: Test) {
        titleLabel.text = test.testName
        
        if let testResult = test.testResult {
            let isPassed = testResult.isPassed()
            let headText = NSMutableAttributedString(string: "\(testResult.numberAnswerCorrect ?? 0)")
            headText.addAttribute(.font, value: UIFont.systemFont(ofSize: 22, weight: .medium), range: NSRange(location: 0, length: headText.length))
            let tailText = NSMutableAttributedString(string: "/\(testResult.totalQuestion ?? 0)")
            tailText.addAttribute(.font, value: UIFont.systemFont(ofSize: 16, weight: .regular), range: NSRange(location: 0, length: tailText.length))
            headText.append(tailText)
            gradeLabel.attributedText = headText
            gradeLabel.textColor = isPassed ? UIColor(named: "PassedColor") : UIColor(named: "FailedColor")
            
        } else {
            gradeLabel.text = "Chưa có\nkết quả"
            gradeLabel.textColor = UIColor(named: "GrayColor2")!
        }
    }
}
