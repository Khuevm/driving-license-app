//
//  FooterQuestionControlView.swift
//  dvsa
//
//  Created by Khai Vuong on 05/02/2022.
//

import Foundation
import UIKit

protocol FooterQuestionControlViewDelegate: AnyObject {
    func footerQuestionControlViewDidTapNextButton(_ view: FooterQuestionControlView)
    func footerQuestionControlViewDidTapPreviousButton(_ view: FooterQuestionControlView)
}

class FooterQuestionControlView: UIView {
    weak var delegate: FooterQuestionControlViewDelegate?
    
    var isEnableNextButton = false {
        didSet {
            nextButton.isHidden = !isEnableNextButton
        }
    }
    
    var isEnablePreviousButton = false {
        didSet {
            previousButton.isHidden = !isEnablePreviousButton
        }
    }
    
    var process: CGFloat = 0 {
        didSet {
            reloadlayout()
        }
    }
    
    // MARK: - IBOutlet
    @IBOutlet var contentView: UIView!
    @IBOutlet weak var previousButton: UIButton!
    @IBOutlet weak var nextButton: UIButton!
    
    // MARK: - LifeCycle
    override init(frame: CGRect) {
        super.init(frame: frame)
        customInit()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        customInit()
    }
    
    private func customInit() {
        loadViewIfNeed()
        
        nextButton.setImage(UIImage(named: "ic_arrow_right_large")?.withRenderingMode(.alwaysTemplate), for: .normal)
        nextButton.tintColor = .white
        nextButton.cornerRadius = 22
        nextButton.backgroundColor = UIColor.init(rgb: 0x4877D8)
        
        
        previousButton.setImage(UIImage(named: "ic_arrow_left_large")?.withRenderingMode(.alwaysTemplate), for: .normal)
        previousButton.tintColor = UIColor.init(rgb: 0x4877D8)
        previousButton.cornerRadius = 22
        previousButton.borderColor = UIColor.init(rgb: 0x4877D8)
        previousButton.borderWidth = 1
    }
    
    private func loadViewIfNeed() {
        if contentView == nil {
            contentView = loadNib()
            addSubview(contentView)
            contentView.fitSuperviewConstraint()
        }
    }
    
    // MARK: - IBAction
    @IBAction func previousButtonDidTap(_ sender: Any) {
        delegate?.footerQuestionControlViewDidTapPreviousButton(self)
    }
    
    @IBAction func nextButtonDidTap(_ sender: Any) {
        delegate?.footerQuestionControlViewDidTapNextButton(self)
    }
    
    // MARK: - Helper
    private func reloadlayout() {
        
    }
}
