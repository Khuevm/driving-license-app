//
//  ProgressGradientView.swift
//  dvsa
//
//  Created by DuNT on 27/01/2022.
//

import UIKit

class ProgressGradientView: UIView {
    
    var progress: CGFloat = 0 {
        didSet {
            updateLayout()
        }
    }
    
    var gradientColors : [CGColor] = [] {
        didSet {
            self.gradient.colors = gradientColors
        }
    }

    override init(frame: CGRect) {
        super.init(frame: frame)
        customInit()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        customInit()
    }
    
    var statusView = UIView()
    var statusContainerView = UIView()
    private var gradient = CAGradientLayer()
    private var statusViewWidthConstraint: NSLayoutConstraint!
    private var isFirst = true
    
    override func layoutSubviews() {
        if isFirst {
            statusView.frame = bounds
            gradient.frame = statusView.bounds
            
            gradient.startPoint = CGPoint(x: 0, y: 0)
            gradient.endPoint = CGPoint(x: 1, y: 0)
            gradient.locations = [0, 1]
            statusView.layer.addSublayer(gradient)
            isFirst = false
        }
    }
    
    private func customInit() {
        backgroundColor = UIColor(rgb: 0xEAEAEA)
        
        addSubview(statusContainerView)
        statusContainerView.translatesAutoresizingMaskIntoConstraints = false
        statusViewWidthConstraint = statusContainerView.widthAnchor.constraint(equalToConstant: 0)
        NSLayoutConstraint.activate([
            statusContainerView.topAnchor.constraint(equalTo: topAnchor),
            statusContainerView.leadingAnchor.constraint(equalTo: leadingAnchor),
            statusContainerView.bottomAnchor.constraint(equalTo: bottomAnchor),
            statusViewWidthConstraint
        ])
        
        statusContainerView.addSubview(statusView)
        statusContainerView.clipsToBounds = true
        
        cornerRadius = frame.height / 2
        statusContainerView.cornerRadius = frame.height / 2
        
//        gradient.frame = statusView.bounds
//        gradient.startPoint = CGPoint(x: 0, y: 0)
//        gradient.endPoint = CGPoint(x: 1, y: 0)
//        gradient.locations = [0, 1]
//        statusView.layer.addSublayer(gradient)
    }
    
    func updateLayout() {
        if progress.isNaN || progress.isInfinite {
            progress = 0
            return
        }
        statusViewWidthConstraint.constant = progress * frame.width
        layoutIfNeeded()
    }

}
