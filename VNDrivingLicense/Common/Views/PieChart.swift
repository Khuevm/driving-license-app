//
//  PieChart.swift
//  VNDrivingLicense
//
//  Created by DuNT on 17/04/2022.
//

import UIKit

class PieChart: UIView {

    var correctColor: UIColor = UIColor(named: "PassedColor")! {
        didSet {
            setNeedsDisplay()
        }
    }
    
    var wrongColor: UIColor = UIColor(named: "FailedColor")! {
        didSet {
            setNeedsDisplay()
        }
    }
    
    var correctProgress: CGFloat = 0 {
        didSet {
            setNeedsDisplay()
        }
    }
    
    var wrongProgress: CGFloat = 0 {
        didSet {
            setNeedsDisplay()
        }
    }
    
    var lineWidth: CGFloat = 7 {
        didSet {
            setNeedsDisplay()
        }
    }
    
    override func draw(_ rect: CGRect) {
        let center = CGPoint.init(x: rect.width / 2, y: rect.height / 2)
        
        let startAngleCorrect = -CGFloat.pi / 2
        let endAngleCorrect = startAngleCorrect + (CGFloat.pi * 2 * correctProgress)
        let startAngleWrong = endAngleCorrect
        let endAngleWrong = startAngleWrong + (CGFloat.pi * 2 * wrongProgress)
        
        // draw Background
        let bgPath = UIBezierPath.init(arcCenter: center, radius: rect.width / 2 - lineWidth/2, startAngle: startAngleCorrect, endAngle: startAngleCorrect + (CGFloat.pi * 2), clockwise: true)
        bgPath.lineWidth = lineWidth
        UIColor.init(rgb: 0xEAEDF3).setStroke()
        bgPath.stroke()
        // draw Progress
        let wrongPath = UIBezierPath.init(arcCenter: center, radius: rect.width / 2  - lineWidth/2, startAngle: startAngleWrong, endAngle: endAngleWrong, clockwise: true)
        wrongPath.lineWidth = lineWidth
        wrongPath.lineCapStyle = .round
        wrongColor.setStroke()
        wrongPath.stroke()
        
        let correctPath = UIBezierPath.init(arcCenter: center, radius: rect.width / 2  - lineWidth/2, startAngle: startAngleCorrect, endAngle: endAngleCorrect, clockwise: true)
        correctPath.lineWidth = lineWidth
        correctPath.lineCapStyle = .round
        correctColor.setStroke()
        correctPath.stroke()
    }
}

