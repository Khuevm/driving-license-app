//
//  MigrateDatabase.swift
//  dmv
//
//  Created by Thom Vu on 28/06/2021.
//

import Foundation
class MigrateDatabase {
    var currentVersionDatabase: Int = 1
    
    var oldVersionDatabase: Int {
        get {
            return UserDefaults.standard.integer(forKey: "oldVersionDatabase")
        }
        set {
            UserDefaults.standard.setValue(newValue, forKey: "oldVersionDatabase")
        }
    }
    
    init()  {
        
    }
    
    func migrateIfNeed() {
        
        if oldVersionDatabase == 0 {
            migrateDatabaseFromVersion0()
        }
        
        oldVersionDatabase = currentVersionDatabase
    }
    
    // MARK: - Migrate from Version 0
    private func migrateDatabaseFromVersion0() {
        
    }
    
    
}
